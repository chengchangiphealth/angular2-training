import { Injectable } from '@angular/core';

@Injectable()
export class MathService {
    state1: number = 0;
    getRandomInt(min: number, max: number) {
        this.state1 = min;
        return Math.round(Math.random() * (max - min)) + min;
    }
}
