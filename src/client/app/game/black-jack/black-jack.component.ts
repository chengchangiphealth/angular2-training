import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MathService } from '../../shared/service/math.service';
/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'black-jack',
  templateUrl: 'black-jack.component.html',
  styleUrls: ['black-jack.component.css']
})
export class BlackJackComponent {
    @Input() name: string;
    @Output() standed = new EventEmitter();
    points: number = 0;
    currentCard: number = 0;
    finished: boolean = false;

    constructor(private mathService: MathService) {}

    hit() {
        this.currentCard = this.mathService.getRandomInt(1, 13);
        this.points += this.currentCard;
        if (this.points > 21) {
            alert('Game Over');
        }
    }

    stand() {
        this.finished = true;
        this.standed.emit(this.points);
    }
}
