import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ToolbarComponent } from './toolbar/toolbar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RandomComponent } from './random/random.component';
import { NameListService } from './name-list/name-list.service';
import { MathService } from './service/math.service';
import { PlayerService } from './service/player.service';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [CommonModule, RouterModule, FormsModule],
  declarations: [ToolbarComponent, NavbarComponent, RandomComponent],
  exports: [ToolbarComponent, NavbarComponent, RandomComponent,
    CommonModule, FormsModule, RouterModule]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [NameListService, MathService, PlayerService]
    };
  }
}
