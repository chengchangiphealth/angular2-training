import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PlayerService {
    constructor(private _http: Http) {}
    getPlayers(): Observable<ClientPlayerModel[]> {
        return this._http.get('http://www.mocky.io/v2/59dd89161000007006a84d8e')
            .map((res: Response) => res.json())
            .map((data: ServerPlayerModel[]) => data.map((aPlayer) => new ClientPlayerModel(aPlayer)));
    }
}

class ServerPlayerModel {
    constructor(public name: string, public score: string) {}
}

export class ClientPlayerModel {
    name: string;
    score: number;
    constructor(input: ServerPlayerModel) {
        this.name = input.name;
        this.score = parseInt(input.score);
    }
}
