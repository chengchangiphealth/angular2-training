import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './game.component';
import { BlackJackComponent } from './black-jack/black-jack.component';
import { GameRoutingModule } from './game-routing.module';
import { MathService } from '../shared/service/math.service';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  imports: [CommonModule, GameRoutingModule, SharedModule],
  declarations: [GameComponent, BlackJackComponent],
  exports: [GameComponent],
  providers: [MathService]
})
export class GameModule { }
