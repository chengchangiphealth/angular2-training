import { Component } from '@angular/core';
import { MathService } from '../service/math.service';

@Component({
    moduleId: module.id,
    selector: 'sd-random',
    template: `
    <h1>Hello world</h1>
    <input type="number" [(ngModel)]="mix">Mix
    <input type="number" [(ngModel)]="max">Max
    <button (click)="generate()">Random</button>
    <input [value]="result" readonly="true">{{ mix }} ~ {{ max }}
    `,
    styleUrls: ['random.component.css'],
})

export class RandomComponent {
    result: number = 0;
    mix: number = 0;
    max: number = 0;
    constructor(private _mathService: MathService) {}
    generate() {
        this.result = this._mathService.getRandomInt(this.mix, this.max);
    }
}
