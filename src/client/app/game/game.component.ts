import { Component, OnInit } from '@angular/core';
// import { Http, Response } from '@angular/http';
import { PlayerService, ClientPlayerModel } from '../shared/service/player.service';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
    moduleId: module.id,
    selector: 'sd-game',
    templateUrl: 'game.component.html',
    styleUrls: ['game.component.css']
})
export class GameComponent implements OnInit {
    players: ClientPlayerModel[] = [];

    constructor(private playerService: PlayerService) {}

    ngOnInit() {
        this.playerService.getPlayers()
            // .map((res: Response) => res.json())
            .subscribe((data: ClientPlayerModel[]) => {
                this.players = data;
            });
    }

    standed(points: number, player: any) {
        player.score = points;
    }

    // standed1(points: number) {
    //     this.score1 = points;
    // }

    // standed2(points: number) {
    //     this.score2 = points;
    // }

    bothFinished(): boolean {
        let finished = true;
        for (let aPlayer of this.players) {
            if (aPlayer.score === 0) {
                return false;
            }
        }
        return finished;
    }

    getWinner() {
        if (!this.bothFinished()) {
            return 'not known';
        } else {
            let theWinner = '';
            let bestScore = 0;
            for (let aPlayer of this.players) {
                if (bestScore < aPlayer.score) {
                    bestScore = aPlayer.score;
                }
            }
            for (let aPlayer of this.players) {
                if (bestScore === aPlayer.score) {
                    theWinner = aPlayer.name;
                    break;
                }
            }
            return theWinner;
        }
    }
}
